package pl.edu.mimuw.ml347073;

public class Osobistość {
    public static void main(String[] args) {
    	int[][] zna = new int[5][5];
    	for(int i = 0; i <= 4; i++) {
    		zna[i][i] = 1;
    	}
    	zna[0][1] = 1;
    	zna[0][2] = 0;
    	zna[0][3] = 0;
    	zna[0][4] = 0;
    	zna[1][0] = 0;
    	zna[1][2] = 0;
    	zna[1][3] = 0;
    	zna[1][4] = 0;
    	zna[2][0] = 0;
    	zna[2][1] = 1;
    	zna[2][3] = 0;
    	zna[2][4] = 0;
    	zna[3][0] = 0;
    	zna[3][1] = 1;
    	zna[3][2] = 0;
    	zna[3][4] = 0;
    	zna[4][0] = 0;
    	zna[4][1] = 1;
    	zna[4][2] = 0;
    	zna[4][3] = 0;
    	if (czy_jest_osobistość(zna))
    		System.out.println("JEST");
    	else
    		System.out.println("NIE MA");
    }
    static boolean czy_jest_osobistość(int[][] zna) {
    	int k, l;
    	for(k = 0, l = 0; l < 4; ) {
    		if(zna[l][k] == 0)
    			k++;
    		else
    			l++;
    	}
    	int i = 0, j = 0;
    	while(zna[i][k] == 1 && i < 4)
    		i++;
    	for(j = 0; j < 4; )
    		if(j == k || zna[k][j] == 0)
    			j++;
    		else
    			break;
    return(i == 4 && j == 4);
    }
}