package pl.edu.mimuw.ml347073;

public class Wyjątek {
	
	private static void wypisz_dlugosc(String s) {
		System.out.println(s.length());
	}
	
	public static void main(String[] args) throws Exception {
		try {
		wypisz_dlugosc(null);
		} catch (NullPointerException w) {
			w.printStackTrace(System.out);
			throw new Exception(w);
		}
	}

}
