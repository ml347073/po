package pl.edu.mimuw.ml347073;

public class Cosinus extends Wyrażenie_1_argumentowe {

	public Cosinus(Wyrażenie wyr_wewn) {
		wewnętrzne = wyr_wewn;
	}

	@Override
	protected String Symbol() {
		return "cos(";
	}

	@Override
	public double Oblicz(int wartość) {
		return Math.cos(wewnętrzne.Oblicz(wartość));
	}

	@Override
	public Wyrażenie Pochodna_własna(Wyrażenie wyr_wewn) {
		Sinus poch_sinus = new Sinus(wyr_wewn);
		Stała minus_jeden = new Stała(-1);
		Razy pochodna_cosinusa = new Razy(minus_jeden, poch_sinus);
		return pochodna_cosinusa;
	}

}
