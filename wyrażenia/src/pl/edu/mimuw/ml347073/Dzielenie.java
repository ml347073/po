package pl.edu.mimuw.ml347073;

public class Dzielenie extends Wyrażenie_2_argumentowe {
	
	public Dzielenie(Wyrażenie lewy, Wyrażenie prawy){
		lewe = lewy;
		prawe = prawy;
	};

	@Override
	protected String Symbol() {
		return "/";
	}

	@Override
	public double Oblicz(int wartość) {
		return lewe.Oblicz(wartość) / prawe.Oblicz(wartość);
	}

	@Override
	public Wyrażenie Pochodna() {
		Razy lewa_pochodna = new Razy(lewe.Pochodna(), prawe);
		Razy prawa_pochodna = new Razy(lewe, prawe.Pochodna());
		Minus pochodna_licznik = new Minus(lewa_pochodna, prawa_pochodna);
		Razy pochodna_mianownik = new Razy(prawe, prawe);
		Dzielenie pochodna_dzielenia = new Dzielenie(pochodna_licznik, pochodna_mianownik);
		return pochodna_dzielenia;
	}

}
