package pl.edu.mimuw.ml347073;

public class Logarytm extends Wyrażenie_1_argumentowe {
	
	public Logarytm(Wyrażenie wyr_wewn) {
		wewnętrzne = wyr_wewn;
	}

	@Override
	protected String Symbol() {
		return "log(";
	}

	@Override
	public double Oblicz(int wartość) {
		return Math.log(wewnętrzne.Oblicz(wartość));
	}

	@Override
	public Wyrażenie Pochodna_własna(Wyrażenie wyr_wewn) {
		Stała jeden = new Stała(1);
		Dzielenie pochodna_log = new Dzielenie(jeden, wyr_wewn);
		return pochodna_log;
	}

}
