package pl.edu.mimuw.ml347073;

public class Minus extends Wyrażenie_2_argumentowe {
	
	public Minus(Wyrażenie lewy, Wyrażenie prawy){
		lewe = lewy;
		prawe = prawy;
	};

	@Override
	protected String Symbol() {
		return "-";
	}

	@Override
	public double Oblicz(int wartość) {
		return lewe.Oblicz(wartość) - prawe.Oblicz(wartość);
	}

	@Override
	public Wyrażenie Pochodna() {
		Minus pochodna_odejmowania = new Minus(lewe.Pochodna(), prawe.Pochodna());
		return pochodna_odejmowania;
	}

}
