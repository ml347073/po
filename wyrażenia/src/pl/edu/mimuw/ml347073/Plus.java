package pl.edu.mimuw.ml347073;

public class Plus extends Wyrażenie_2_argumentowe {
	
	public Plus(Wyrażenie lewy, Wyrażenie prawy){
		lewe = lewy;
		prawe = prawy;
	};

	@Override
	protected String Symbol() {
		return "+";
	}

	@Override
	public double Oblicz(int wartość) {
		return lewe.Oblicz(wartość) + prawe.Oblicz(wartość);
	}

	@Override
	public Wyrażenie Pochodna() {
		Plus pochodna_dodawania = new Plus(lewe.Pochodna(), prawe.Pochodna());
		return pochodna_dodawania;
	}
	
}
