package pl.edu.mimuw.ml347073;

public class Razy extends Wyrażenie_2_argumentowe {
	
	public Razy(Wyrażenie lewy, Wyrażenie prawy){
		lewe = lewy;
		prawe = prawy;
	};

	@Override
	protected String Symbol() {
		return "*";
	}

	@Override
	public double Oblicz(int wartość) {
		return lewe.Oblicz(wartość) * prawe.Oblicz(wartość);
	}

	@Override
	public Wyrażenie Pochodna() {
		Razy lewa_pochodna = new Razy(lewe.Pochodna(), prawe);
		Razy prawa_pochodna = new Razy(lewe, prawe.Pochodna());
		Plus pochodna_mnożenia = new Plus(lewa_pochodna, prawa_pochodna);
		return pochodna_mnożenia;
	}

}
