package pl.edu.mimuw.ml347073;

public class Sinus extends Wyrażenie_1_argumentowe {

	public Sinus(Wyrażenie wyr_wewn) {
		wewnętrzne = wyr_wewn;
	}

	@Override
	protected String Symbol() {
		return "sin(";
	}

	@Override
	public double Oblicz(int wartość) {
		return Math.sin(wewnętrzne.Oblicz(wartość));
	}

	@Override
	public Wyrażenie Pochodna_własna(Wyrażenie wyr_wewn) {
		Cosinus pochodna_sinusa = new Cosinus(wyr_wewn);
		return pochodna_sinusa;
	}

}
