package pl.edu.mimuw.ml347073;

public class Stała extends Wyrażenie {

	private int wart;
	
	public Stała(int v) {
		wart = v;
	}

	@Override
	public void Wypisz() {
		System.out.print(wart);
	}

	@Override
	public double Oblicz(int wartość) {
		return wart;
	}

	@Override
	public Wyrażenie Pochodna() {
		Stała pochodna_stałej = new Stała(0);
		return pochodna_stałej;
	}

}
