package pl.edu.mimuw.ml347073;

public abstract class Wyrażenie {
    public abstract void Wypisz();
    public abstract double Oblicz(int wartość);
    public abstract Wyrażenie Pochodna();
}
