package pl.edu.mimuw.ml347073;

public abstract class Wyrażenie_1_argumentowe extends Wyrażenie {
	
    protected Wyrażenie wewnętrzne;
    
    protected abstract String Symbol();
    
	@Override
	public void Wypisz() {
	    System.out.print(Symbol());
	    wewnętrzne.Wypisz();
	    System.out.print(")");
	}

	public abstract double Oblicz(int wartość);
	
	public abstract Wyrażenie Pochodna_własna(Wyrażenie wyr_wewn);

	public Wyrażenie Pochodna() {
		Wyrażenie lewa = Pochodna_własna(wewnętrzne);
		Wyrażenie prawa = wewnętrzne.Pochodna();
		Razy pochodna_zewnętrzna = new Razy(lewa, prawa);
		return pochodna_zewnętrzna;
	}

}