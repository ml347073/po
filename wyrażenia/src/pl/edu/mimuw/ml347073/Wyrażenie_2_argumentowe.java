package pl.edu.mimuw.ml347073;

public abstract class Wyrażenie_2_argumentowe extends Wyrażenie {
    protected Wyrażenie lewe, prawe;
    
    protected abstract String Symbol();
    
	@Override
	public void Wypisz() {
		System.out.print("(");
	    lewe.Wypisz();
	    System.out.print(Symbol());
	    prawe.Wypisz();
		System.out.print(")");
	}

	public abstract double Oblicz(int wartość);

	public abstract Wyrażenie Pochodna();
	
}
