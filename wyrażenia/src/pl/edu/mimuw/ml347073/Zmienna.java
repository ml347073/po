package pl.edu.mimuw.ml347073;

public class Zmienna extends Wyrażenie {

	@Override
	public void Wypisz() {
		System.out.print('x');
	}

	@Override
	public double Oblicz(int wartość) {
		return wartość;
	}

	@Override
	public Wyrażenie Pochodna() {
		Stała pochodna_stałej = new Stała(0);
		return pochodna_stałej;
	}
	
}
